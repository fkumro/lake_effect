defmodule LakeEffect.MixProject do
  use Mix.Project

  @target System.get_env("MIX_TARGET") || "host"

  def project do
    [
      app: :lake_effect,
      version: "0.2.0",
      elixir: "~> 1.7",
      target: @target,
      archives: [nerves_bootstrap: "~> 1.3"],
      deps_path: "deps/#{@target}",
      build_path: "_build/#{@target}",
      lockfile: "mix.lock.#{@target}",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: [loadconfig: [&bootstrap/1]],
      deps: deps(),
      # Docs
      name: "LakeEffect",
      source_url: "https://gitlab.com/fkumro/lake_effect",
      homepage_url: "http://fkumro.gitlab.io/lake_effect",
      # The main page in the docs
      docs: [
        main: "LakeEffect",
        logo: "static/anonymous-simple-weather-symbols-5-300px.png",
        extras: []
      ]
    ]
  end

  defp bootstrap(args) do
    Application.start(:nerves_bootstrap)
    Mix.Task.run("loadconfig", args)
  end

  # Run "mix help compile.app" to learn about applications.
  def application, do: application(@target)

  # Specify target specific application configurations
  # It is common that the application start function will start and supervise
  # applications which could cause the host to fail. Because of this, we only
  # invoke LakeEffect.start/2 when running on a target.
  def application("host") do
    [extra_applications: [:logger]]
  end

  def application(_target) do
    [mod: {LakeEffect.Application, []}, extra_applications: [:logger]]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nerves, "~> 1.3", runtime: false},
      {:elixir_ale, "~> 1.1"},
      {:jason, "~> 1.1"},
      {:httpoison, "~> 1.3"}
    ] ++ deps(@target)
  end

  # Specify target specific dependencies
  defp deps("host") do
    [
      {:ex_doc, "~> 0.19", only: :dev, runtime: false}
    ]
  end

  defp deps(target) do
    [
      {:shoehorn, "~> 0.4"},
      {:nerves_runtime, "~> 0.8"},
      {:nerves_network, "~> 0.3"},
      {:nerves_firmware_ssh, "~> 0.3", except: :test},
      {:nerves_time, "~> 0.2.0"}
    ] ++ system(target)
  end

  defp system("rpi3"), do: [{:nerves_system_rpi3, "~> 1.5", runtime: false}]
  defp system("x86_64"), do: [{:nerves_system_x86_64, ">= 0.0.0", runtime: false}]
  defp system(target), do: Mix.raise("Unknown MIX_TARGET: #{target}")
end
