# Lake Effect

Weather station powered by Elixir/Nerves using a Raspberry Pi 3

![Built with Nerves](/static/nerves-badge_200x104_black.png?raw=true "Built with Nerves")

## Setup

Environment variables are required for wireless setup/Thunder Snow before firmware is built.
Use the table below to set the correct variables for your environment.

| Env var name | Description  | Default? |
|---|---|---|
| `NERVES_NETWORK_KEY_MGMT`  | Wireless auth. method.  | Yes, `WPA-PSK`  |
| `NERVES_NETWORK_SSID`  | Network name to connect to.  | No  |
| `NERVES_NETWORK_PSK`  | Network key. | No  |
| `NERVES_FIRMWARE_SSH_KEY` | SSH *public* key to use for SSH connection when flashing remotely. Value is appended to the current users home directory. Not required to begin with a slash.| No |
| `API_KEY`  | Shared API key between Lake Effect and Thunder Snow. | No  |
| `API_URL`  | URL to the Thunder Snow server (`/api/reports` endpoint). | No  |

## Remote Flashing

If you have any ssh keys with passphrases, you may have to isolate the key
used for the weather station in its own directory. In my testing, a key without
a passphrase could not be in the same directory as one with a passphrase. If it
was, then the connection would return an error about not being able to connect
due to ssh key or passphrase.

`--user-dir` should be the directory containing the SSH key set by
`NERVES_FIRMWARE_SSH_KEY`.

```bash
MIX_TARGET=rpi3 mix firmware.push --user-dir=/home/username/.ssh/weather_station 192.168.2.16
```

## Fritzing Wiring Layout

The wiring is documented with Fritzing and can be found in `fritzing/weather_station.fzz`. A
image repsentation is included below for those who don't want to install the
application.

![Fritzing Image](/fritzing/wind-speed-wiring.png "Fritzing wiring layout")