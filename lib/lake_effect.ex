defmodule LakeEffect do
  @moduledoc """
  A project to build a weather station that monitors temperature and wind speed.

  ## Parts List
  - [Rasberry Pi 3](https://www.adafruit.com/product/3055)
  - [MCP3008 - analog to digital conversion](https://www.adafruit.com/product/856)
  - [Barrel jack power input for a bread board](https://www.adafruit.com/product/373)
  - [High Temp Waterproof DS18B20 Digital temperature](https://www.adafruit.com/product/642)
  - [Anemometer Wind Speed Sensor w/Analog Voltage Output](https://www.adafruit.com/product/1733)
  """
end
