defmodule LakeEffect.WeatherReport do
  alias LakeEffect.Sensors.WindSpeed
  alias LakeEffect.Sensors.Temperature
  alias LakeEffect.Clients.ThunderSnow
  alias LakeEffect.Convert

  @typedoc "Map describing the data read from the sensors"
  @type t :: %{wind_speed: number, temperature: number}

  @doc """
  Collects readings from the temperature and wind speed sensors. Values are
  imperial.
  """
  @spec collect :: WeatherReport.t()
  def collect do
    %{wind_speed: WindSpeed.read(), temperature: Temperature.read()}
    |> Convert.to_imperial()
  end

  @doc """
  Saves the sensor data using the ThunderSnow client.
  """
  @spec save(WeatherReport.t()) :: term
  def save(sensor_data) do
    sensor_data
    |> Convert.to_json()
    |> ThunderSnow.send()
  end
end
