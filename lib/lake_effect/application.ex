defmodule LakeEffect.Application do
  @moduledoc false
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      supervisor(LakeEffect.Sensors.Supervisor, []),
      supervisor(LakeEffect.Clients.Supervisor, []),
      worker(LakeEffect.Jobs.Server, []),
      {NervesNTP, [sync_on_start: true]}
    ]

    opts = [strategy: :one_for_one, name: LakeEffect.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
