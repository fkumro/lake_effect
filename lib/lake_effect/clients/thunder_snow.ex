defmodule LakeEffect.Clients.ThunderSnow do
  @moduledoc """
  Functions for persisting weather readings to a server running Thunder Snow.
  You can find more information out about the Thunder Snow project on its
  [Gitlab page](https://gitlab.com/fkumro/thunder_snow).
  """

  @doc """
  Send weather readings to the server.
  """
  @spec send(WeatherReport.t()) :: term
  defdelegate send(weather_report), to: LakeEffect.Clients.ThunderSnow.Server, as: :send
end
