defmodule LakeEffect.Clients.ThunderSnow.Server do
  @moduledoc false
  use GenServer
  alias LakeEffect.Clients.ThunderSnow.Impl

  # Client API
  @spec send(WeatherReport.t()) :: term
  def send(weather_report) do
    GenServer.call(:thunder_snow, {:send, weather_report})
  end

  # Server API
  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :thunder_snow)
  end

  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_call({:send, data}, _from, state) do
    Impl.send(data)
    {:reply, :ok, state}
  end
end
