defmodule LakeEffect.Clients.ThunderSnow.Impl do
  @moduledoc false

  @spec send(binary()) :: tuple()
  def send(weather_report_json) do
    HTTPoison.start()
    url = Application.get_env(:lake_effect, __MODULE__)[:url]
    api_key = Application.get_env(:lake_effect, __MODULE__)[:api_key]
    IO.puts("URL: #{url}")
    IO.puts("API KEY: #{api_key}")
    headers = [{"Content-Type", "application/json"}, {"Authorization", "Bearer #{api_key}"}]

    case HTTPoison.post(url, weather_report_json, headers) do
      {:ok, %HTTPoison.Response{status_code: 201}} ->
        {:ok, :created}

      {:ok, %HTTPoison.Response{status_code: 403}} ->
        {:error, :forbidden}

      {:ok, %HTTPoison.Response{status_code: 500}} ->
        {:error, :internal_server_error}

      {:error, %HTTPoison.Error{reason: reason}} ->
        {:error, reason}

      _ ->
        {:error, :uncaught}
    end
  end
end
