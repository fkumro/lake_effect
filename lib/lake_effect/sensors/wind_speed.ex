defmodule LakeEffect.Sensors.WindSpeed do
  @moduledoc """
  Functions for reading the wind speed from an anemometer wind speed sensor
  w/analog voltage output connected to an MCP3008 analog to digital conversion
  chip over SPI.

  Using the channel which the data wire from the sensor is connected to, a
  binary payload can be constructed to retrieve the value for counts output by
  the sensor. Counts can then be converted through multiple steps to miles per
  hour. The chip layout is shown below to assist which determining which channel
  the sensor data wire is connected to.

  ![MCP3008 Chip Layout](./assets/mcp3008-layout.png)

  The order and value of the bits to send to the MCP3008 are detailed below,
  reference the MCU Transmitted Data section.


  ![MCP3008 Binary Communication Specs](./assets/mcp3008-communication.png)

  A total of 24 bits are required to be sent, and if configured correctly, 24
  bits will be received (shown via the MCU Received Data section). The first
  8 bits are seven `0`'s followed by a `1`, which is the start bit. Following
  those bits, are `4` configuration bits, these are critical to receiving data
  that is not garbage. Using the chart below, the bits should be `1000`. If you
  are using a different sensor, or are using a different channel, your bits will
  not match. Adjust them to your specific configuration.

  ![MCP3008 Binary Setup](./assets/mcp3008-configuration.png)

  """

  @doc """
  Read the current wind speed in miles per hour
  """
  @spec read() :: Float.t()
  defdelegate read(), to: LakeEffect.Sensors.WindSpeed.Server, as: :read
end
