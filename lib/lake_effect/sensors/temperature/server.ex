defmodule LakeEffect.Sensors.Temperature.Server do
  @moduledoc false
  use GenServer
  alias LakeEffect.Sensors.Temperature.Comm

  # Client API
  def read() do
    GenServer.call(:temperature_sensor, :read)
  end

  # Server API
  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :temperature_sensor)
  end

  @impl true
  def init(_) do
    {:ok, %{}}
  end

  @impl true
  def handle_call(:read, _from, _state) do
    {:reply, Comm.read(), %{}}
  end
end
