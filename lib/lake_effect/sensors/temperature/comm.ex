defmodule LakeEffect.Sensors.Temperature.Comm do
  @moduledoc false

  @spec read() :: float
  def read() do
    as_binary()
    |> parse_temp()
  end

  defp as_binary() do
    Application.get_env(:lake_effect, __MODULE__)[:sensor_path]
    |> File.read!()
  end

  defp parse_temp(data) do
    {temp, _} =
      Regex.run(~r/t=(\d+)/, data)
      |> List.last()
      |> Float.parse()

    temp / 1000
  end
end
