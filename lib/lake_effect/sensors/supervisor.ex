defmodule LakeEffect.Sensors.Supervisor do
  use Supervisor

  def start_link() do
    Supervisor.start_link(__MODULE__, name: __MODULE__)
  end

  @impl true
  def init(_) do
    children = [
      worker(LakeEffect.Sensors.Temperature.Server, []),
      worker(LakeEffect.Sensors.WindSpeed.Server, [])
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
