defmodule LakeEffect.Sensors.WindSpeed.Server do
  @moduledoc false
  use GenServer
  alias ElixirALE.SPI
  alias LakeEffect.Sensors.WindSpeed.Comm

  # Client API
  def read() do
    GenServer.call(:wind_speed_sensor, :read)
  end

  # Server API
  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :wind_speed_sensor)
  end

  @impl true
  def init(_) do
    {:ok, pid} = SPI.start_link("spidev0.0")
    {:ok, %{ale: pid}}
  end

  @impl true
  def handle_call(:read, _from, state) do
    {:reply, Comm.read(state[:ale]), state}
  end
end
