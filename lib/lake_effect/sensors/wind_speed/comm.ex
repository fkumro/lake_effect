defmodule LakeEffect.Sensors.WindSpeed.Comm do
  alias LakeEffect.Convert
  alias ElixirALE.SPI

  def read(pid) do
    pid
    |> spi_transfer(transmission_payload())
    |> spi_read
    |> Convert.from_counts_to_volts()
    |> Convert.from_voltage_to_ms()
    |> Convert.from_ms_to_mph()
  end

  defp transmission_payload(), do: <<0x01, 0x80, 0x00>>

  defp spi_transfer(pid, payload) do
    pid
    |> SPI.transfer(payload)
  end

  defp spi_read(<<_::size(14), counts::size(10)>>) do
    counts
  end
end
