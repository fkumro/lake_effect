defmodule LakeEffect.Jobs.Server do
  @moduledoc false
  use GenServer
  alias LakeEffect.WeatherReport

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :jobs_server)
  end

  def init(_) do
    if enabled?() do
      schedule_initial_job()
      {:ok, nil}
    else
      :ignore
    end
  end

  def handle_info(:perform, state) do
    WeatherReport.collect()
    |> WeatherReport.save()

    schedule_next_job()

    {:noreply, state}
  end

  defp schedule_initial_job() do
    # wait 60 seconds to allow networking to connect
    Process.send_after(self(), :perform, 60_000)
  end

  defp schedule_next_job() do
    Process.send_after(self(), :perform, 30_000)
  end

  defp enabled?() do
    Application.get_env(:lake_effect, __MODULE__)[:enabled]
  end
end
