use Mix.Config

config :lake_effect, LakeEffect.Jobs.Server, enabled: true

# wireless configuration setting
config :nerves_network, regulatory_domain: "US"
key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA-PSK"

config :nerves_network, :default,
  wlan0: [
    ssid: System.get_env("NERVES_NETWORK_SSID"),
    psk: System.get_env("NERVES_NETWORK_PSK"),
    key_mgmt: String.to_atom(key_mgmt)
  ],
  eth0: [
    ipv4_address_method: :dhcp
  ]

# firmware flashing over ssh config
config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(Path.join(System.user_home!(), System.get_env("NERVES_FIRMWARE_SSH_KEY")))
  ]

config :lake_effect, LakeEffect.Sensors.Temperature.Comm,
  sensor_path: "/sys/bus/w1/devices/28-0000081bfd1c/w1_slave"
