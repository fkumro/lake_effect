defmodule ConvertTest do
  use ExUnit.Case, async: true
  doctest LakeEffect.Convert

  describe "from_counts_to_volts/1" do
    test "150 counts converts correctly" do
      assert(LakeEffect.Convert.from_counts_to_volts(150) == 0.4838709677419355)
    end

    test "0 counts converts correctly" do
      assert(LakeEffect.Convert.from_counts_to_volts(0) == 0.0)
    end
  end

  describe "from_celsius_to_fahrenheit/1" do
    test "1 C equals 33.8 F" do
      assert(LakeEffect.Convert.from_celsius_to_fahrenheit(1) == 33.8)
    end

    test "40 C equals 104 F" do
      assert(LakeEffect.Convert.from_celsius_to_fahrenheit(40) == 104.0)
    end
  end
end
